from model.Enet import *
from data_processing import *
import time
import numpy as np
import cv2
import datetime
def rescale_frame(frame, percent=75):
	width = int(frame.shape[1] * percent/ 100)
	height = int(frame.shape[0] * percent/ 100)
	dim = (width, height)
	return cv2.resize(frame, dim, interpolation =cv2.INTER_AREA)

model = load_model('/home/himanshu/ivlabs/adb/road_bce.hdf5')

#cv2.namedWindow('mask', cv2.WINDOW_NORMAL)
cap = cv2.VideoCapture('/home/himanshu/ivlabs/adb/20191001_181427.mp4')
fourcc = cv2.VideoWriter_fourcc(*'XVID')
out = cv2.VideoWriter('test2.avi',fourcc, 30, (1280,720))
alpha = 0.7
while(cap.isOpened()): 
	# Capture frame-by-frame
	ret, frame = cap.read()
	if ret == True:
		# frame = cv2.flip(frame, 0)
		# frame = cv2.flip(frame, 1)
		frame1 = frame
		# frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
		# frame = rescale_frame(frame, percent = 80)

		# Our operations on the frame come here

		frame = frame/255
		frame = cv2.resize(frame, (512,512))
		frame = np.reshape(frame,(1,)+frame.shape)
		results = model.predict(frame, batch_size = 1,verbose=1)
		results = np.squeeze(results)
		results = results*255
		results = results.astype(np.uint8)
		# ret2,results = cv2.threshold(results,0,255,cv2.THRESH_OTSU)

		temp = np.zeros((512,512, 3), np.uint8)
		temp[:,:,1] = results
		temp = cv2.resize(temp, (1280,720))

		segmented = cv2.addWeighted(frame1, alpha, temp, (1-alpha), 0.0)
		# out.write(segmented)
		cv2.imshow('img', segmented)
	if cv2.waitKey(1) & 0xFF == ord('q'):
		break
# When everything done, release the capture
cap.release()
out.release()
cv2.destroyAllWindows()